all:
	ocamlbuild -cflags -w,Ale -libs nums Src/Main.native

clean:
	ocamlbuild -clean
	rm -rf *~
