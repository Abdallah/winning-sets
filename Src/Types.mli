type candidate = int
type preference = int array
type profile = preference list
type coalition = candidate list 

module type SolutionConcept =
sig
  val set : profile -> coalition -> bool
  val set_theta : profile -> coalition -> int
  val name : string
end
