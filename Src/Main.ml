(*module Sh = Generation.Shuffle*)

module DominatingSets = Experiments.Sets (Solution.Dominating)
module WinningSets = Experiments.Sets (Solution.Condorcet)

let test_decisive () =
  let ks = [2; 3; 4] and ms = [5; 20] and ns = [50; 400; 500; 1000; 5000] in
  let samples = 10_000 in
  WinningSets.multi_decisive ks ms ns samples;
(*  DominatingSets.multi_distrib_max ks ms ns samples;*)
  ()

let test_theta_distrib () =
  let samples = 1_000 in
  WinningSets.multi_theta_distrib [1] [30] [100] samples;
  WinningSets.multi_theta_distrib [2] [30] [100] samples;
  WinningSets.multi_theta_distrib [3] [30] [100] samples;
  WinningSets.multi_theta_distrib [4] [30] [100] samples;
  ()

let test_dim_distrib () =
  let ns = [10; 11; 20; 21; 100; 101] in
(*  WinningSets.multi_dim_distrib [4] [15; 50; 100] ns 1000;*)
  DominatingSets.multi_dim_distrib [6]  [15] ns 1000;
  DominatingSets.multi_dim_distrib [5]  [50] ns 100;
  DominatingSets.multi_dim_distrib [4] [100] ns 10;
  ()

let test_proba_set () =
  let samples = 1_000 in
  WinningSets.multi_proba_set [1] [30] [50] samples;
  WinningSets.multi_proba_set [2] [30] [50] samples;
  WinningSets.multi_proba_set [3] [30] [30] samples;
  WinningSets.multi_proba_set [4] [30] [25] samples;
  ()

(*let _ = test_decisive ()*)
(*let _ = test_theta_distrib ()*)
(*let _ = test_dim_distrib ()*)
(*let _ = test_proba_set ()*)
