val remove_list : 'a list -> 'a list -> 'a list
val range : int -> int -> int list
val min_list : int list -> int
val max_list : int list -> int
val all_among : int -> int list -> int list list
val count_all_among : int -> int -> int
val print_to_file : string -> string -> unit
val list_count : ('a -> bool) -> 'a list -> int

val perform : ('a -> unit) -> 'a option -> unit
val list_map : ('a -> 'b) -> 'a list -> 'b list
