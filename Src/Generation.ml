(*open Printf*)

(*let append l1 l2 = List.rev_append (List.rev l1) l2
let flatten l = List.fold_left append [] l

let fst_n n l = 
  let rec aux m accu ll = match ll with
    | [] -> accu
    | a :: r -> if m <= 0 then accu else aux (m - 1) (a :: accu) r in
  aux n [] l

let all_inserts x l = 
  let rec aux result accu r =
    let result' = List.rev_append (x :: accu) r :: result in
    match r with
      | [] -> result'
      | a :: rest -> aux result' (a :: accu) rest in
  aux [] [] l

let all_orders l = List.fold_left (fun acc a -> flatten (Misc.list_map (all_inserts a) acc)) [[]] l

let add_list las lbss =
(*  printf "las:%d, lbss:%d\n" (List.length las) (List.length lbss); flush_all ();*)
(*  let las = fst_n 1000 las
  and lbss = fst_n 10000 lbss in*)
  let joined = Misc.list_map (fun a -> Misc.list_map (fun b -> a :: b) lbss) las in
(*  printf "blabla\n"; flush_all ();*)
  flatten joined*)

module type Shuffle =
sig
  val random_profile : m:int -> int -> Types.profile
  val name : string
end

module ImpartialCulture : Shuffle =
struct
  type t = { candidates : Types.candidate array }
  let name = "ic"

  let make l = { candidates = Array.of_list l }
  let get_preference t =
    let a = t.candidates in
    let result = Array.copy a in
    let stop = ref (Array.length a) in
    let swap i j = 
      let temp = a.(i) in
      a.(i) <- a.(j);
      a.(j) <- temp in
    let step () =
      let i = Random.int !stop in
      swap i (!stop - 1);
      decr stop in
    while !stop > 0 do
      step ()
    done;
    result

  let random_profile ~m voters =
    let candidates = Misc.range 1 m in
    let generator = make candidates in
    let get_pref _ = get_preference generator in
    List.map get_pref (Misc.range 1 voters)
end

module ImpartialAnonymousCulture : Shuffle =
struct
  type t = { candidates : Types.candidate list;
             nb_candidates : int;
             additional : Types.profile;
             nb_additional : int;
             current : Types.profile }
  let name = "iac"

  let make m = { candidates = Misc.range 1 m;
                 nb_candidates = m;
                 additional = [];
                 nb_additional = 0;
                 current = [] }

(*  let fact_big_int n =
    let rec aux accu n =
      if n <= 0 then accu
      else aux (Big_int.mult_big_int accu (Big_int.big_int_of_int n)) (n - 1) in
    aux Big_int.unit_big_int n

  let rec log_max n =
    let maxi = Big_int.big_int_of_int max_int in
    let (quot, _) = Big_int.quomod_big_int n maxi in
    if Big_int.eq_big_int quot Big_int.zero_big_int then 1 else 1 + log_max quot
    
  let _ = 
    let n = 100 in
    let f = fact_big_int n in
    let l = log_max f in
    eprintf "%s %d\n" (Big_int.string_of_big_int f) l*)

(*  let get_preference t =
    let a = t.candidates in
    let result = Array.copy a in
    let stop = ref (Array.length a) in
    let swap i j = 
      let temp = a.(i) in
      a.(i) <- a.(j);
      a.(j) <- temp in
    let step () =
      let i = Random.int !stop in
      swap i (!stop - 1);
      decr stop in
    while !stop > 0 do
      step ()
    done;
    result

  let random_profile candidates voters =
    let generator = make candidates in
    let get_pref _ = get_preference generator in
    List.map get_pref (Misc.range 1 voters)*)
  let random_profile ~m _ = ignore (make m); assert false
end
