open Printf

let rec range x y =
  if x > y then []
  else x :: range (x + 1) y

let rec remove_first e l = match l with
  | [] -> assert false
  | a :: r when a = e -> r
  | a :: r -> a :: remove_first e r

let remove_list es l = List.fold_left (fun accu e -> remove_first e accu) l es


let rec all_among_aux x l size =
  if x <= 0 then [[]]
  else if x > size then []
  else match l with
    | [] -> assert false
    | a :: r ->
      let without_a = all_among_aux x r (size - 1) in
      let with_a = all_among_aux (x - 1) r (size - 1) in
      let with_a = List.rev_map (fun ll -> a :: ll) with_a in
      List.rev (List.rev_append without_a with_a)

let all_among k l = all_among_aux k l (List.length l)

let rec count_all_among_aux k m = if k <= 0 then 1 else m * count_all_among_aux (k - 1) (m - 1)
let rec fact n = if n <= 0 then 1 else n * fact (n - 1)
let count_all_among k m = count_all_among_aux k m / fact k

let max_list l = match l with
  | [] -> assert false
  | a :: r -> List.fold_left max a r

let min_list l = match l with
  | [] -> assert false
  | a :: r -> List.fold_left min a r

let print_to_file filename string =
  let file = open_out filename in
  fprintf file "%s" string;
  close_out file

let list_count predicate l = List.length (List.filter predicate l)

let perform f = function
  | None -> ()
  | Some a -> f a

let list_map f l = List.rev (List.rev_map f l)
