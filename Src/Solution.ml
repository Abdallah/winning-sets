(*open Printf*)

let opponents profile coalition =
  assert (profile <> []);
  let m = Array.length (List.hd profile) in
  Misc.remove_list coalition (Misc.range 1 m)

let ranked_higher x opponent (preference : Types.preference) =
  assert (x <> opponent);
  preference.(x - 1) > preference.(opponent - 1)
    
let ranked_higher_coalition coalition opponent (preference : Types.preference) =
  List.exists (fun x -> ranked_higher x opponent preference) coalition

let majority profile candidate opponent =
  let (want, _) = List.partition (ranked_higher candidate opponent) profile in
  List.length want
  

(** [count_higher c o p] returns the number of elements of [c] that
    are better ranked than [o] according to voter [p] *)
let count_higher coalition opponent (preference : Types.preference) =
  let higher = List.filter (fun candidate -> ranked_higher candidate opponent preference) coalition in
  List.length higher

(** [count_highers c o p] returns for each index [i] between 0 and
    [|c|] the maximal theta that could be used and still lead to [o]
    being beaten by coalition [c] with conjonctivity [i] according to
    profile [p] *)
let count_highers coalition opponent (profile : Types.profile) =
  let k = List.length coalition in
  let scores = Array.make (k + 1) 0 in
  let add_one preference =
    let c = count_higher coalition opponent preference in
    for i = 0 to c do
      scores.(i) <- 1 + scores.(i)
    done in
  List.iter add_one profile;
  scores

(** [coalitions_scores c o p] returns for each index [i] between 0 and
    [|c|] the maximal theta that could be used and still lead to every
    opponent in [o] being beaten by coalition [c] with conjonctivity
    [i] according to profile [p] *)
let coalitions_scores coalition (profile : Types.profile) =
  let opps = opponents profile coalition in
  assert (profile <> []);
  let n = List.length profile in
  let k = List.length coalition in
  let scores = Array.make (k + 1) n in
  let add_one opponent =
    let scores' = count_highers coalition opponent profile in
    for i = 0 to k do
      scores.(i) <- min scores.(i) scores'.(i)
    done in
  List.iter add_one opps;
  scores

module Dominating =
struct
  let set profile coalition =
    let opps = opponents profile coalition in
    let n = List.length profile in
    let dominated opponent = List.exists (fun candidate -> 2 * majority profile candidate opponent > n) coalition in
    List.for_all dominated opps

  let set_theta profile coalition =
    let opps = opponents profile coalition in
    let dominated opponent = Misc.max_list (List.map (fun candidate -> majority profile candidate opponent) coalition) in
    Misc.min_list (List.map dominated opps)

  let name = "dom"
end

module Condorcet =
struct
  let majority_theta (profile : Types.profile) coalition opponent =
    let (want, _) = List.partition (ranked_higher_coalition coalition opponent) profile in
    List.length want

  let majority_coal (profile : Types.profile) coalition opponent =
    let n = List.length profile in
    2 * majority_theta profile coalition opponent > n

  let set (profile : Types.profile) coalition =
    let opps = opponents profile coalition in
    List.for_all (majority_coal profile coalition) opps

  let set_theta (profile : Types.profile) coalition =
    let opps = opponents profile coalition in
    Misc.min_list (List.map (majority_theta profile coalition) opps)

  let name = "cws"
end
