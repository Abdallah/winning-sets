module Example1 :
sig
  val test : unit -> unit
end

module RandomCWS :
sig
  val test : unit -> unit
end

module ExistenceCWS :
sig
  val test : unit -> unit
end

module MeasureTheta :
sig
  val test : unit -> unit
end

module MeasureThetaVariableSize :
sig
  val test : unit -> unit
end

module CountDistrib :
sig
  val test : unit -> unit
end

module Dimension :
sig
  val test : unit -> unit
end

module Sets (SC : Types.SolutionConcept) :
sig
  (** Distribution of the dimension across random profiles.*)
  val multi_dim_distrib   : ks:int list -> ms:int list -> ns:int list -> samples:int -> unit
 
  (** Distribution of the number of theta-WS/theta-DS for theta maximal (under the constraint that there is at least one elected set.*)
  val multi_decisive   : ks:int list -> ms:int list -> ns:int list -> samples:int -> unit

  (** Probability that a random coalition in a random profile is CWS/Domination set.*)
  val multi_proba_set     : ks:int list -> ms:int list -> ns:int list -> samples:int -> unit

  (** Distribution of theta_max.*)
  val multi_theta_distrib : ks:int list -> ms:int list -> ns:int list -> samples:int -> unit
end
