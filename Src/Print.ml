open Printf

let int = sprintf "%d"
let float = sprintf "%f"
let list' s1 s2 s3 pr l = sprintf "%s%s%s" s1 (String.concat s2 (List.map pr l)) s3
let list pr l = list' "[" "; " "]" pr l
let array pr l = list' "[|" "; " "|]" pr (Array.to_list l)
let matrix pr l = list' "[|" "\n  " "|]" (array pr) (Array.to_list l)
let couple pr1 pr2 (x, y) = sprintf "(%s, %s)" (pr1 x) (pr2 y)
let unlines pr l = list' "" "\n" "" pr l
let unspaces pr l = list' "" " " "" pr l
let array_unspaces pr l = list' "" " " "" pr (Array.to_list l)

let preference = array (sprintf "%d")
let profile = list preference
let coalition = list (sprintf "%d")
