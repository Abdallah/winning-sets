module type Shuffle =
sig
  val random_profile : m:int -> int -> Types.profile
  val name : string
end

module ImpartialCulture : Shuffle
