val int : int -> string
val float : float -> string
val list : ('a -> string) -> 'a list -> string
val unlines : ('a -> string) -> 'a list -> string
val unspaces : ('a -> string) -> 'a list -> string
val array : ('a -> string) -> 'a array -> string
val array_unspaces : ('a -> string) -> 'a array -> string
val matrix : ('a -> string) -> 'a array array -> string
val couple : ('a -> string) -> ('b -> string) -> 'a * 'b -> string

val preference : Types.preference -> string
val profile : Types.profile -> string
val coalition : Types.coalition -> string
(*let coalitions = list coalition
let coal_opp = couple coalition coalition
let coal_opps = list coal_opp*)
