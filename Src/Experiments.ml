open Printf

let foi = float_of_int
let iof = int_of_float

let random_profile = Generation.ImpartialCulture.random_profile

let all_coalitions_opponents_aux k m = 
  assert (0 < k);
  assert (0 < m);
  let candidates = Misc.range 1 m in
  try Misc.all_among k candidates with exn -> eprintf "%s\n" (Printexc.to_string exn); assert false

let all_coalitions_opponents =
  let results = Hashtbl.create 17 in
  let f k m =
    try Hashtbl.find results (k, m)
    with Not_found ->
      let to_store = all_coalitions_opponents_aux k m in
      Hashtbl.add results (k, m) to_store;
      to_store in
  f

let get_all_winning_sets n m profile =
  let all_coal = all_coalitions_opponents n m in
  List.filter (Solution.Condorcet.set profile) all_coal

module Example1 =
struct
  let m = 15
  let profile =
    [[|  1;  2;  3;  4;  5;   6;  7;  8;  9; 10;  11; 12; 13; 14; 15|];
     [|  5;  1;  2;  3;  4;  10;  6;  7;  8;  9;  15; 11; 12; 13; 14|];
     [|  4;  5;  1;  2;  3;   9; 10;  6;  7;  8;  14; 15; 11; 12; 13|];
     [|  3;  4;  5;  1;  2;   8;  9; 10;  6;  7;  13; 14; 15; 11; 12|];
     [|  2;  3;  4;  5;  1;   7;  8;  9; 10;  6;  12; 13; 14; 15; 11|];
     
     [| 11; 12; 13; 14; 15;   1;  2;  3;  4;  5;   6;  7;  8;  9; 10|];
     [| 15; 11; 12; 13; 14;   5;  1;  2;  3;  4;  10;  6;  7;  8;  9|];
     [| 14; 15; 11; 12; 13;   4;  5;  1;  2;  3;   9; 10;  6;  7;  8|];
     [| 13; 14; 15; 11; 12;   3;  4;  5;  1;  2;   8;  9; 10;  6;  7|];
     [| 12; 13; 14; 15; 11;   2;  3;  4;  5;  1;   7;  8;  9; 10;  6|];
     
     [|  6;  7;  8;  9; 10;  11; 12; 13; 14; 15;   1;  2;  3;  4;  5|];
     [| 10;  6;  7;  8;  9;  15; 11; 12; 13; 14;   5;  1;  2;  3;  4|];
     [|  9; 10;  6;  7;  8;  14; 15; 11; 12; 13;   4;  5;  1;  2;  3|];
     [|  8;  9; 10;  6;  7;  13; 14; 15; 11; 12;   3;  4;  5;  1;  2|];
     [|  7;  8;  9; 10;  6;  12; 13; 14; 15; 11;   2;  3;  4;  5;  1|]]
      
  let test () =
    let winn1 = get_all_winning_sets 1 m profile in
    let winn2 = get_all_winning_sets 2 m profile in
    let winn3 = get_all_winning_sets 3 m profile in 
    ignore (winn1, winn2, winn3);
    assert false 
(*    printf "\n\nExample 1 from paper:\nwinning 1 %s\nwinning 2 %s\nwinning 3 [%s; ...]\n" 
      (Print.coal_opps winn1) 
      (Print.coal_opps winn2)
      (Print.coal_opp (List.hd winn3))*)
end

module RandomCWS =
struct
  let one_win coalition m n =
    let profile = random_profile m n in
    Solution.Condorcet.set profile coalition

  let number_win k m n samples =
    let coalition = Misc.range 1 k in
    let t _ = one_win coalition m n in
    Misc.list_count t (Misc.range 1 samples)
    
  let print_nb_win k m n samples =
    let nsuccess = number_win k m n samples in
    let probability = foi nsuccess /. foi samples in
    sprintf "%d %d %d %f\n" k m n probability

  let voters_fixed filename ~n samples =
    for ncoalition = 1 to 4 do
      let filename = sprintf "%s_k%d_n%d.plot" filename ncoalition n in
      let file = open_out filename in
      for c = 0 to 100 do
(*        let m = c * (iof (sqrt (foi c))) + ncoalition + 1 in*)
        let m = 5 * c + ncoalition + 1 in
        let result = print_nb_win ncoalition m n samples in
        fprintf file "%s%!" result;
      done;
      close_out file
    done

  let _candid_fixed filename ~m samples =
    for ncoalition = 1 to 4 do
      let filename = sprintf "%s_k%d_m%d.plot" filename ncoalition m in
      let file = open_out filename in
      for v = 0 to 20 do
        let n = v * v + 1 in
        let result = print_nb_win ncoalition m n samples in
        fprintf file "%s%!" result;
      done;
      close_out file
    done

  let test () =
    let n = 50 in
(*    let m = 30 in*)
    let filename = sprintf "random_cws/random_cws" in
    voters_fixed filename n 1_000;
(*    _candid_fixed filename m 10_000;*)
    ()
end

module ExistenceCWS =
struct

  let exist_winning k m n =
    let coalitions = all_coalitions_opponents k m in
    let profile = random_profile m n in
    List.exists (Solution.Condorcet.set profile) coalitions

  let number_win k m n samples =
    let t _ = exist_winning k m n in
    let (samples, bad) = List.partition t (Misc.range 1 samples) in
    let nb_bad = List.length bad in
    if nb_bad > 0 && k > 1 then eprintf "counter-example : %d%!" nb_bad;
    List.length samples
    
  let print_nb_win k m n samples =
    let nsuccess = number_win k m n samples in
    let percent = 100. *. foi nsuccess /. foi samples in
    sprintf "%d %d %d %f\n" k m n percent

  let voters_fixed filename ~n samples =
    for ncoalition = 1 to 1 do
      let filename = sprintf "%s_k%d_n%d.plot" filename ncoalition n in
      let file = open_out filename in
      for c = 0 to 49 do
        let m = c * (iof (sqrt (foi c))) + ncoalition + 1 in
        let result = print_nb_win ncoalition m n samples in
        fprintf file "%s%!" result;
      done;
      close_out file
    done

  let candid_fixed filename ~m samples =
    for ncoalition = 1 to 1 do
      let filename = sprintf "%s_k%d_m%d.plot" filename ncoalition m in
      let file = open_out filename in
      for v = 0 to 50 do
        let n = v * v + 1 in
        let result = print_nb_win ncoalition m n samples in
        fprintf file "%s%!" result;
      done;
      close_out file
    done

  let test () =
    let filename = "exist_cws" in
    let m = 30 in
    let n = 20 in
    voters_fixed filename n 10_000;
    candid_fixed filename m 10_000;
    ()
end

module MeasureTheta =
struct

  let get_one_theta k m n =
    let coalitions = all_coalitions_opponents k m in
    let profile = random_profile m n in
    Misc.max_list (List.map (Solution.Condorcet.set_theta profile) coalitions)

  let theta_distribution k m n samples =
    let theta_array = Array.make (n + 1) 0 in
    let theta () = get_one_theta k m n in
    let add_theta () =
      let t = theta () in
      assert (t >= 0);
      assert (t <= n);
      theta_array.(t) <- theta_array.(t) + 1 in
    for _i = 1 to samples do
      add_theta ()
    done;
    theta_array

  let dynamic_theta_distribution filename k m n samples =
    let theta_array = Array.make (n + 1) 0 in
    let theta () = get_one_theta k m n in
    let print_line i = sprintf "%f %d" (foi i /. foi n) theta_array.(i) in
    let print () =
      let content = Print.unlines print_line (Misc.range 0 n) in
      Misc.print_to_file filename content in
    let add_theta () =
      let t = theta () in
      assert (t >= 0);
      assert (t <= n);
      theta_array.(t) <- theta_array.(t) + 1 in
    for i = 1 to samples do
      if i mod 20 = 0 then print ();
      add_theta ()
    done;
    print ();
    let average = ref 0. in
    for i = 0 to n do
      average := !average +. foi i *. foi theta_array.(i)
    done;
    average := !average /. foi n /. foi samples;
    eprintf "moyenne : %f" !average
    
  let _print_distribution k m n samples =
    let distrib = theta_distribution k m n samples in
    Print.unlines (fun i -> sprintf "%d %d" i distrib.(i)) (Misc.range 0 n)

  let test () =
    let m = 20 in
    let n = 50 in
    for k = 1 to 3 do
      let filename = sprintf "theta_distrib_k%d_m%d_n%d.plot" k m n in
      dynamic_theta_distribution filename k m n 500
    done
end

module MeasureThetaVariableSize =
struct

  let get_thetas k_max m n =
    let result = Array.make k_max [||] in
    for k = 1 to k_max do
      let coalitions = all_coalitions_opponents k m in
      let profile = random_profile m n in
      let scores = Array.make k 0 in
      let add_one coalition = 
        let scores' = Solution.coalitions_scores coalition profile in
        for i = 0 to k - 1 do
          scores.(i) <- max scores.(i) scores'.(i)
        done in
      List.iter add_one coalitions;
      result.(k - 1) <- scores
    done;
    eprintf "%s\n%!" (Print.matrix Print.int result);
    result.(k_max - 1)

  let dynamic_theta_distribution filename k m n samples =
    let theta_matrix = Array.make_matrix k (n + 1) 0 in
    let thetas () = get_thetas k m n in
    let print_line size ti = sprintf "%f %d" (foi ti /. foi n) theta_matrix.(size).(ti) in
    let print_size size =
      let content = Print.unlines (print_line size) (Misc.range 0 n) in
      Misc.print_to_file (sprintf "%s_size%d.plot" filename size) content in
    let print () = List.iter print_size (Misc.range 0 (k - 1)) in
    let add_theta () =
      let t = thetas () in
      eprintf "theta k%d m%d n%d :\n%s\n%!" k m n (Print.array Print.int t);
      for i = 0 to k - 1 do
        theta_matrix.(i).(t.(i)) <- theta_matrix.(i).(t.(i)) + 1
      done in
    for i = 1 to samples do
      if i mod 10 = 0
      then (eprintf "sample %d\n%!" i; print ());
      add_theta ()
    done;
    print ()

  let test () =
    let m = 30 in
    let n = 100 in
    for k = 1 to 3 do
      eprintf "k = %d\n" k;
      let filename = sprintf "data/variable_size/k%d_m%d_n%d" k m n in
      try dynamic_theta_distribution filename k m n 100
      with exn -> eprintf "%s\n" (Printexc.to_string exn); assert false
    done
end

module CountDistrib =
struct
  (* 100 / 20
     20 / 100
     50 / 50 *)
  let count_winning k m n theta =
    let coalitions = all_coalitions_opponents k m in
    let profile = random_profile m n in
    let results = List.map (Solution.Condorcet.set_theta profile) coalitions in
    Misc.list_count ((<) theta) results

  let count_max k m n _ =
    let coalitions = all_coalitions_opponents k m in
    let profile = random_profile m n in
    let results = List.map (Solution.Condorcet.set_theta profile) coalitions in
    let maxi = Misc.max_list results in
    Misc.list_count ((=) maxi) results

  let dynamic_count_distribution count filename k m ns samples =
    let coalitions = all_coalitions_opponents k m in
    let nb_coalitions = Misc.count_all_among k m in
    assert (nb_coalitions = List.length coalitions);
    let count_array = Array.make_matrix (nb_coalitions + 1) (Array.length ns) 0 in
    let theta n = iof (foi k /. (foi k +. 1.) *. foi n) in
    let count_one () = Array.map (fun n -> count k m n (theta n)) ns in
    let add_array a = Array.iteri (fun i t -> count_array.(t).(i) <- count_array.(t).(i) + 1) a in
    let print_line i =
      let n_results = Print.array_unspaces Print.int count_array.(i) in 
      sprintf "%d %f %s" i (foi i /. foi nb_coalitions) n_results in
    let print () = Misc.print_to_file filename (Print.unlines print_line (Misc.range 0 nb_coalitions)) in
    for i = 1 to samples do
      if (i mod 20) = 0 then print ();
      let a = count_one () in
      add_array a
    done;
    print ()

  let test () =
    let k = 2 in
    let n = [|3; 4; 5; 6; 7; 8; 9; 10; 20; 30; 40; 50; 100; 200; 300; 400; 500; 1000; 2000|] in
    let m = 20 in
    let filename = sprintf "data/count_distrib/count_distrib_k%d_m%d.plot" k m in
    dynamic_count_distribution count_winning filename k m n 200;
    let filename = sprintf "data/count_max/count_max_k%d_m%d.plot" k m in
    dynamic_count_distribution count_max filename k m n 200;
(*    let m = 50 in
    let filename = sprintf "count_distrib/count_distrib_k%d_m%d.plot" k m in
    dynamic_count_distribution filename k m n 1_000;*)
(*    let m = 100 in
    let filename = sprintf "count_distrib/count_distrib_k%d_m%d.plot" k m in
    dynamic_count_distribution filename k m n 1_000;*)

end

module Sets (SC : Types.SolutionConcept) =
struct

  let get_dimension k_max m n =
    let profile = random_profile m n in
    let rec aux k =
      if k >= 7 then eprintf "profile %d:\n%s\n%!" k (Print.profile profile);
      if k > k_max then k_max + 1
      else
      (assert (k <= k_max);
       let coalitions = all_coalitions_opponents k m in
       if List.exists (SC.set profile) coalitions then k
       else aux (k + 1)) in
    try aux 1 with exn -> eprintf "%s\n" (Printexc.to_string exn); assert false

  let dim_distrib k_max m n samples =
    let filename = sprintf "bla/%s/dim_distrib/m%03d_n%04d.plot" SC.name m n in
    let dim_array = Array.make (k_max + 1) 0 in
    let print_line index = sprintf "%d %d" index dim_array.(index - 1) in
    let print () =
      eprintf "dim m%d n%d : %s\n%!" m n (Print.array Print.int dim_array);
      Misc.print_to_file filename (Print.unlines print_line (Misc.range 1 (k_max + 1))) in
    for i = 1 to 100 * samples do
      if i mod samples = 0 then (eprintf "sample %d\n%!" i; print ());
      let d = get_dimension k_max m n in
      dim_array.(d - 1) <- dim_array.(d - 1) + 1
    done;
    print ()

  let count_sets k m n =
    let profile = random_profile m n in
    let coalitions = all_coalitions_opponents k m in
    Misc.list_count (SC.set profile) coalitions

(*  let count_distrib k m n samples =
    let filename = sprintf "data/%s/count_distrib/k%d_m%03d_n%04d.plot" SC.name k m n in
    let nb_coalitions = Misc.count_all_among k m in
    let count_array = Array.make (nb_coalitions + 1) 0 in
    let print_line index = sprintf "%d %d" index count_array.(index) in
    let print info =
      Misc.perform (fun i -> eprintf "sample %d: count m%d n%d : %s\n%!" i m n (Print.array Print.int count_array)) info;
      Misc.print_to_file filename (Print.unlines print_line (Misc.range 0 nb_coalitions)) in
    for i = 1 to samples do
      if i mod 100 = 0 then print None;
      if i mod 1000 = 0 then print (Some i);
      let d = count_sets k m n in
      count_array.(d) <- count_array.(d) + 1
    done;
    print None*)

  let proba_set k m n_max samples =
    let filename = sprintf "data/%s/proba_set/k%d_m%03d.plot" SC.name k m in
    let nb_coalitions = Misc.count_all_among k m in
    let count_array = Array.make n_max 0. in
    let print_line n = sprintf "%d %f" (1 + 2 * n) count_array.(n) in
    for n = 0 to n_max - 1 do
      let print info =
        Misc.perform (fun i -> eprintf "sample %d: proba k%d m%d n%d : %s\n%!" i k m (n + 1) (Print.array Print.float count_array)) info;
        Misc.print_to_file filename (Print.unlines print_line (Misc.range 0 (n_max - 1))) in
      for i = 1 to samples do
        if i mod 100 = 0 then print None;
        if i mod 1000 = 0 then print (Some i);
        let d = count_sets k m (1 + 2 * n) in
        count_array.(n) <- count_array.(n) +. foi d /. foi samples /. foi nb_coalitions
      done;
    print None
    done

  let thetas_profile k m n =    
    let profile = random_profile m n in
    let coalitions = all_coalitions_opponents k m in
    let thetas = Misc.list_map (SC.set_theta profile) coalitions in
    let theta_max = Misc.max_list thetas in
    theta_max, thetas

  let theta_distrib k m n samples =
    let filename = sprintf "data/%s/theta_distrib/k%d_m%03d_n%04d.plot" SC.name k m n in
    let count_array = Array.make n 0 in
    let print_line index = sprintf "%f %d" (foi (index + 1) /. foi n) count_array.(index) in
    let print info =
      Misc.perform (fun i -> eprintf "sample %d: %s theta distrib k%d m%d n%d : %s\n%!" i SC.name k m n (Print.array Print.int count_array)) info;
      Misc.print_to_file filename (Print.unlines print_line (Misc.range 0 (n - 1))) in
    for i = 1 to samples do
      if i mod 100 = 0 then print None;
      if i mod 1000 = 0 then print (Some i);
      let theta = fst (thetas_profile k m n) in
      assert (theta > 0 && theta <= n);
      count_array.(theta - 1) <- count_array.(theta - 1) + 1
    done;
    print None

  let count_max_theta k m n =
    let theta_max, thetas = thetas_profile k m n in
    Misc.list_count ((=) theta_max) thetas

  let decisive k m n samples =
    let filename = sprintf "data/%s/decisive/k%d_m%03d_n%04d.plot" SC.name k m n in
    let nb_coalitions = Misc.count_all_among k m in
    let count_array = Array.make (nb_coalitions + 1) 0 in
    let print_line index = sprintf "%d %d" index count_array.(index) in
    let print info =
      Misc.perform (fun i -> eprintf "sample %d: %s decisive k%d m%d n%d : %s\n%!" i SC.name k m n (Print.array Print.int count_array)) info;
      Misc.print_to_file filename (Print.unlines print_line (Misc.range 1 nb_coalitions)) in
    for i = 1 to samples do
      if i mod 100 = 0 then print None;
      if i mod 1000 = 0 then print (Some i);
      let d = count_max_theta k m n in
      count_array.(d) <- count_array.(d) + 1
    done;
    print None

  let multi_dim_distrib   ~ks ~ms ~ns ~samples = List.iter (fun k -> List.iter (fun m -> List.iter (fun n -> dim_distrib   k m n samples) ns) ms) ks
(*  let multi_count_distrib ~ks ~ms ~ns ~samples = List.iter (fun k -> List.iter (fun m -> List.iter (fun n -> count_distrib k m n samples) ns) ms) ks*)
  let multi_decisive      ~ks ~ms ~ns ~samples = List.iter (fun k -> List.iter (fun m -> List.iter (fun n -> decisive      k m n samples) ns) ms) ks
  let multi_proba_set     ~ks ~ms ~ns ~samples = List.iter (fun k -> List.iter (fun m -> List.iter (fun n -> proba_set     k m n samples) ns) ms) ks
  let multi_theta_distrib ~ks ~ms ~ns ~samples = List.iter (fun k -> List.iter (fun m -> List.iter (fun n -> theta_distrib k m n samples) ns) ms) ks
end

module Dimension =
struct

  let get_dimension m n =
    let coalitions1 = all_coalitions_opponents 1 m in
    let coalitions2 = all_coalitions_opponents 2 m in
    let profile = random_profile m n in
    let test = Solution.Condorcet.set profile in
    if List.exists test coalitions1 then 1
    else if List.exists test coalitions2 then 2
    else
      (Misc.print_to_file "counter_examples" (Print.profile profile);
       eprintf "!dim 3!\n";
       3)

  let dim_distrib m n samples =
    let filename = sprintf "data/cws/dim_distrib_m%d_n%d" m n in
    let dim_array = Array.make 3 0 in
    let print_line index = sprintf "%d %d" index dim_array.(index - 1) in
    let print () =
      eprintf "dim m%d n%d : %s\n%!" m n (Print.array Print.int dim_array);
      let content = Print.unlines print_line (Misc.range 1 3) in
      Misc.print_to_file (sprintf "%s.plot" filename) content in
    let add_dim () =
      let d = get_dimension m n in
      dim_array.(d - 1) <- dim_array.(d - 1) + 1 in
    for i = 1 to samples do
      if i mod 100 = 0 then (eprintf "sample %d\n%!" i; print ());
      add_dim ()
    done;
    print ()
    
  let dynamic_dimension_distribution filename m ns samples =
    let count_array = Array.make_matrix (Array.length ns) 3 0 in
    let dim_array_n () = Array.map (fun n -> get_dimension m n) ns in
    let add_array a = Array.iteri (fun n d -> count_array.(n).(d - 1) <- count_array.(n).(d - 1) + 1) a in
    let print_dim d =
      let l = Array.map (fun i -> i.(d - 1)) count_array in
      let l = Array.to_list l in
      let n_results = Print.unspaces Print.int l in 
      sprintf "%d %s" d n_results in
    let print () : unit =
      let n_choice = Print.unspaces Print.int (Array.to_list ns) in
      let content = sprintf "m = %d\n  %s\n%s" m n_choice (Print.unlines print_dim [1; 2; 3]) in
      Misc.print_to_file filename content in
    let add_count () =
      let a = dim_array_n () in
      add_array a in
    for i = 1 to samples do
      if (i mod 20) = 0 then print ();
      add_count ()
    done;
    print ()

  let test () =
    let __ () = ignore (dynamic_dimension_distribution "" 0 [||] 0) in
(*    let ns = [|10; 11; 20; 21; 100; 101|] in
    let m = 15 in
    let filename = sprintf "dimen_distrib/dimen_distrib_m%d.plot" m in
    dynamic_dimension_distribution filename m ns 1_000_000;
    let m = 50 in
    let filename = sprintf "dimen_distrib/dimen_distrib_m%d.plot" m in
    dynamic_dimension_distribution filename m ns 1_000_000;
    let m = 100 in
    let filename = sprintf "dimen_distrib/dimen_distrib_m%d.plot" m in
    dynamic_dimension_distribution filename m ns 100_000;
    let m = 500 in
    let filename = sprintf "dimen_distrib/dimen_distrib_m%d.plot" m in
    dynamic_dimension_distribution filename m ns 10_000;*)
    let n = 100
    and m = 50 in
    dim_distrib m n 10_000;
    ()

end
