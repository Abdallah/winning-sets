README file for winning-sets, please also read the LICENSE file (BSD 2-Clause License).

May 2014

General
-------

This repository contains code to experiment around electing a set of alternatives.
It is an updated version of the code used to generate the experimental data in the following paper:

* Edith Elkind, Jérôme Lang, and Abdallah Saffidine. Choosing collectively optimal sets of alternatives based on the Condorcet criterion. In Toby Walsh, editor, 22nd International Joint Conference on Artificial Intelligence (IJCAI), pages 186-191, Barcelona, Spain, July 2011. AAAI Press.

Latest Version
--------------

Details of the latest version can be obtained via the project website https://bitbucket.org/Abdallah/winning-sets.
Suggestions and patches welcomed.

Requirement
-----------

The OCaml system version 3.11.2 or above.

Using the code
--------------

Edit `Main.ml` to prepare the desired experiments, compile with `make`, run `./Main.native`.
